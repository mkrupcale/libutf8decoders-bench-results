# Table of contents

 - [Benchmark input categories](#benchmark-input-categories)
   - [High ASCII input](#high-ascii-input)
   - [Medium ASCII input](#medium-ascii-input)
   - [Low ASCII input](#low-ascii-input)
 - [Benchmark setup](#benchmark-setup)
 - [Overview of benchmark results](#overview-of-benchmark-results)
   - [Clang 8.0](#clang-80)
     - [Stress tests](#stress-tests)
     - [Wikipedia pages](#wikipedia-pages)
   - [GCC 9.1](#gcc-91)
     - [Stress tests](#stress-tests-1)
     - [Wikipedia pages](#wikipedia-pages-1)
   - [Summary](#summary)
     - [High ASCII input](#high-ascii-input-1)
     - [Medium ASCII input](#medium-ascii-input-1)
     - [Low ASCII input](#low-ascii-input-1)
   - [Conclusions](#conclusions)
 - [Fastest decoders](#fastest-decoders)
   - [Clang 8.0](#clang-80-1)
     - [Stress tests](#stress-tests-2)
     - [Wikipedia pages](#wikipedia-pages-2)
   - [GCC 9.1](#gcc-91-1)
     - [Stress tests](#stress-tests-3)
     - [Wikipedia pages](#wikipedia-pages-3)
   - [Summary](#summary-1)
     - [High ASCII input](#high-ascii-input-2)
     - [Medium ASCII input](#medium-ascii-input-2)
     - [Low ASCII input](#low-ascii-input-2)
   - [Conclusions](#conclusions-1)

# Benchmark input categories

The benchmark input files are categorized according to how much relative ASCII content they contain.

## High ASCII input

The benchmarks with primarily ASCII input include:

 - `stress/0`
 - `English_language-en`

and to an extent

 - `Portuguese_language-pt`
 - `Swedish_language-sv`

## Medium ASCII input

The benchmarks with medium ASCII input are:

 - `stress/2`

## Low ASCII input

The benchmarks with low ASCII input are:

 - `stress/1`
 - `Hindi_language-hi`
 - `Japanese_language-ja`
 - `Korean_language-ko`
 - `Russian_language-ru`
 - `Chinese_language-zh`

# Benchmark setup

The benchmark results are presented using both GCC 9.1 and Clang 8.0 compilers in C++17 mode with `-O2` optimizations and running on an Intel Xeon E3-1230 CPU. Ten outer iterations of each benchmark were performed, and the means and standard errors are plotted.

# Overview of benchmark results

## Clang 8.0

### Stress tests

![clang++ 8.0 C++17 Xeon E3-1230 stress/0](clang++-8.0-17/clang++_8.0_C++17_Xeon_E3-1230_f01_8_CPUs_@_3600_MHz_stress-0_cpu.svg)

![clang++ 8.0 C++17 Xeon E3-1230 stress/1](clang++-8.0-17/clang++_8.0_C++17_Xeon_E3-1230_f02_8_CPUs_@_3600_MHz_stress-1_cpu.svg)

![clang++ 8.0 C++17 Xeon E3-1230 stress/2](clang++-8.0-17/clang++_8.0_C++17_Xeon_E3-1230_f03_8_CPUs_@_3600_MHz_stress-2_cpu.svg)

### Wikipedia pages

![clang++ 8.0 C++17 Xeon E3-1230 English_language-en](clang++-8.0-17/clang++_8.0_C++17_Xeon_E3-1230_f04_8_CPUs_@_3600_MHz_wiki-English_language-en_cpu.svg)

![clang++ 8.0 C++17 Xeon E3-1230 Hindi_language-hi](clang++-8.0-17/clang++_8.0_C++17_Xeon_E3-1230_f05_8_CPUs_@_3600_MHz_wiki-Hindi_language-hi_cpu.svg)

![clang++ 8.0 C++17 Xeon E3-1230 Japanese_language-ja](clang++-8.0-17/clang++_8.0_C++17_Xeon_E3-1230_f06_8_CPUs_@_3600_MHz_wiki-Japanese_language-ja_cpu.svg)

![clang++ 8.0 C++17 Xeon E3-1230 Korean_language-ko](clang++-8.0-17/clang++_8.0_C++17_Xeon_E3-1230_f07_8_CPUs_@_3600_MHz_wiki-Korean_language-ko_cpu.svg)

![clang++ 8.0 C++17 Xeon E3-1230 Portuguese_language-pt](clang++-8.0-17/clang++_8.0_C++17_Xeon_E3-1230_f08_8_CPUs_@_3600_MHz_wiki-Portuguese_language-pt_cpu.svg)

![clang++ 8.0 C++17 Xeon E3-1230 Russian_language-ru](clang++-8.0-17/clang++_8.0_C++17_Xeon_E3-1230_f09_8_CPUs_@_3600_MHz_wiki-Russian_language-ru_cpu.svg)

![clang++ 8.0 C++17 Xeon E3-1230 Swedish_language-sv](clang++-8.0-17/clang++_8.0_C++17_Xeon_E3-1230_f10_8_CPUs_@_3600_MHz_wiki-Swedish_language-sv_cpu.svg)

![clang++ 8.0 C++17 Xeon E3-1230 Chinese_language-zh](clang++-8.0-17/clang++_8.0_C++17_Xeon_E3-1230_f11_8_CPUs_@_3600_MHz_wiki-Chinese_language-zh_cpu.svg)

## GCC 9.1

### Stress tests

![g++ 9.1 C++17 Xeon E3-1230 stress/0](g++-9.1-17/g++_9.1_C++17_Xeon_E3-1230_f01_8_CPUs_@_3600_MHz_stress-0_cpu.svg)

![g++ 9.1 C++17 Xeon E3-1230 stress/1](g++-9.1-17/g++_9.1_C++17_Xeon_E3-1230_f02_8_CPUs_@_3600_MHz_stress-1_cpu.svg)

![g++ 9.1 C++17 Xeon E3-1230 stress/2](g++-9.1-17/g++_9.1_C++17_Xeon_E3-1230_f03_8_CPUs_@_3600_MHz_stress-2_cpu.svg)

### Wikipedia pages

![g++ 9.1 C++17 Xeon E3-1230 English_language-en](g++-9.1-17/g++_9.1_C++17_Xeon_E3-1230_f04_8_CPUs_@_3600_MHz_wiki-English_language-en_cpu.svg)

![g++ 9.1 C++17 Xeon E3-1230 Hindi_language-hi](g++-9.1-17/g++_9.1_C++17_Xeon_E3-1230_f05_8_CPUs_@_3600_MHz_wiki-Hindi_language-hi_cpu.svg)

![g++ 9.1 C++17 Xeon E3-1230 Japanese_language-ja](g++-9.1-17/g++_9.1_C++17_Xeon_E3-1230_f06_8_CPUs_@_3600_MHz_wiki-Japanese_language-ja_cpu.svg)

![g++ 9.1 C++17 Xeon E3-1230 Korean_language-ko](g++-9.1-17/g++_9.1_C++17_Xeon_E3-1230_f07_8_CPUs_@_3600_MHz_wiki-Korean_language-ko_cpu.svg)

![g++ 9.1 C++17 Xeon E3-1230 Portuguese_language-pt](g++-9.1-17/g++_9.1_C++17_Xeon_E3-1230_f08_8_CPUs_@_3600_MHz_wiki-Portuguese_language-pt_cpu.svg)

![g++ 9.1 C++17 Xeon E3-1230 Russian_language-ru](g++-9.1-17/g++_9.1_C++17_Xeon_E3-1230_f09_8_CPUs_@_3600_MHz_wiki-Russian_language-ru_cpu.svg)

![g++ 9.1 C++17 Xeon E3-1230 Swedish_language-sv](g++-9.1-17/g++_9.1_C++17_Xeon_E3-1230_f10_8_CPUs_@_3600_MHz_wiki-Swedish_language-sv_cpu.svg)

![g++ 9.1 C++17 Xeon E3-1230 Chinese_language-zh](g++-9.1-17/g++_9.1_C++17_Xeon_E3-1230_f11_8_CPUs_@_3600_MHz_wiki-Chinese_language-zh_cpu.svg)

## Summary

### High ASCII input

For content with large amounts of ASCII input, the fastest decoders are the KEWB, ICU and Python and Boost.Text decoders. In particular, the KEWB SSE decoders are the fastest, followed by ICU `U8_NEXT_UNSAFE`, KEWB `Fast{Big,Small}TableConvert`, ICU `U8_NEXT`, and Python. The slowest decoders are LLVM, branchless, utf8proc and codecvt.

### Medium ASCII input

For content with medium amounts of ASCII input, the fastest decoders are again the KEWB, ICU, and Python implementations. In this case, Boost.Text is no longer among the best performers, and the KEWB SSE decoders do not perform as well as the KEWB Fast decoders. Furthermore, the ICU `U8_NEXT_UNSAFE` and `u_strFromUTF8Lenient` implementations are at the forefront, followed closely by KEWB `FastBigTableConvert`, ICU `U8_NEXT`, and Python decoders, with the order of those depending on whether Clang or GCC were used. The slowest decoders are again LLVM, branchless, utf8proc, and either Boost.Text or AV for Clang or GCC, respectively.

### Low ASCII input

For content with small amounts of ASCII input, the fastest decoders are again the KEWB, ICU, and Python implementations. Like the medium ASCII case, the unsafe ICU implementations are the fastest, followed by KEWB `FastBigTableConvert`, ICU `U8_NEXT`, and Python, with the order depending on whether Clang or GCC were used. The slowest implementations are utf8proc, LLVM, AV, and branchless, with Boost.Text the slowest decoder for Clang.

## Conclusions

The fastest all-around implementations are probably the KEWB `FastBigTableConvert`, ICU `U8_NEXT`, and Python decoders. If the input is primarily ASCII, the KEWB SSE decoders can give a significant advantage over the non-vectorized decoders, but they tend to perform worse than the fastest non-vectorized decoders for non-ASCII input. If the input sequence is valid UTF-8, the unsafe ICU decoders also perform really well. The slowest decoders are probably LLVM, branchless, and utf8proc.

# Fastest decoders

Having gotten a better idea of which benchmarks perform the best, we focus now on some of the fastest decoders, including the MK implementations which are based on the ICU `U8_NEXT` and KEWB `FastBigTableConvert` implementations. The results are consistent with the overall benchmarks, but this gives a closer look at the comparison between these and the MK implementations.

## Clang 8.0

### Stress tests

![clang++ 8.0 C++17 Xeon E3-1230 stress/0](clang++-8.0-17/fastest/clang++_8.0_C++17_Xeon_E3-1230_f01_8_CPUs_@_3600_MHz_stress-0_cpu.svg)

![clang++ 8.0 C++17 Xeon E3-1230 stress/1](clang++-8.0-17/fastest/clang++_8.0_C++17_Xeon_E3-1230_f02_8_CPUs_@_3600_MHz_stress-1_cpu.svg)

![clang++ 8.0 C++17 Xeon E3-1230 stress/2](clang++-8.0-17/fastest/clang++_8.0_C++17_Xeon_E3-1230_f03_8_CPUs_@_3600_MHz_stress-2_cpu.svg)

### Wikipedia pages

![clang++ 8.0 C++17 Xeon E3-1230 English_language-en](clang++-8.0-17/fastest/clang++_8.0_C++17_Xeon_E3-1230_f04_8_CPUs_@_3600_MHz_wiki-English_language-en_cpu.svg)

![clang++ 8.0 C++17 Xeon E3-1230 Hindi_language-hi](clang++-8.0-17/fastest/clang++_8.0_C++17_Xeon_E3-1230_f05_8_CPUs_@_3600_MHz_wiki-Hindi_language-hi_cpu.svg)

![clang++ 8.0 C++17 Xeon E3-1230 Japanese_language-ja](clang++-8.0-17/fastest/clang++_8.0_C++17_Xeon_E3-1230_f06_8_CPUs_@_3600_MHz_wiki-Japanese_language-ja_cpu.svg)

![clang++ 8.0 C++17 Xeon E3-1230 Korean_language-ko](clang++-8.0-17/fastest/clang++_8.0_C++17_Xeon_E3-1230_f07_8_CPUs_@_3600_MHz_wiki-Korean_language-ko_cpu.svg)

![clang++ 8.0 C++17 Xeon E3-1230 Portuguese_language-pt](clang++-8.0-17/fastest/clang++_8.0_C++17_Xeon_E3-1230_f08_8_CPUs_@_3600_MHz_wiki-Portuguese_language-pt_cpu.svg)

![clang++ 8.0 C++17 Xeon E3-1230 Russian_language-ru](clang++-8.0-17/fastest/clang++_8.0_C++17_Xeon_E3-1230_f09_8_CPUs_@_3600_MHz_wiki-Russian_language-ru_cpu.svg)

![clang++ 8.0 C++17 Xeon E3-1230 Swedish_language-sv](clang++-8.0-17/fastest/clang++_8.0_C++17_Xeon_E3-1230_f10_8_CPUs_@_3600_MHz_wiki-Swedish_language-sv_cpu.svg)

![clang++ 8.0 C++17 Xeon E3-1230 Chinese_language-zh](clang++-8.0-17/fastest/clang++_8.0_C++17_Xeon_E3-1230_f11_8_CPUs_@_3600_MHz_wiki-Chinese_language-zh_cpu.svg)

## GCC 9.1

### Stress tests

![g++ 9.1 C++17 Xeon E3-1230 stress/0](g++-9.1-17/fastest/g++_9.1_C++17_Xeon_E3-1230_f01_8_CPUs_@_3600_MHz_stress-0_cpu.svg)

![g++ 9.1 C++17 Xeon E3-1230 stress/1](g++-9.1-17/fastest/g++_9.1_C++17_Xeon_E3-1230_f02_8_CPUs_@_3600_MHz_stress-1_cpu.svg)

![g++ 9.1 C++17 Xeon E3-1230 stress/2](g++-9.1-17/fastest/g++_9.1_C++17_Xeon_E3-1230_f03_8_CPUs_@_3600_MHz_stress-2_cpu.svg)

### Wikipedia pages

![g++ 9.1 C++17 Xeon E3-1230 English_language-en](g++-9.1-17/fastest/g++_9.1_C++17_Xeon_E3-1230_f04_8_CPUs_@_3600_MHz_wiki-English_language-en_cpu.svg)

![g++ 9.1 C++17 Xeon E3-1230 Hindi_language-hi](g++-9.1-17/fastest/g++_9.1_C++17_Xeon_E3-1230_f05_8_CPUs_@_3600_MHz_wiki-Hindi_language-hi_cpu.svg)

![g++ 9.1 C++17 Xeon E3-1230 Japanese_language-ja](g++-9.1-17/fastest/g++_9.1_C++17_Xeon_E3-1230_f06_8_CPUs_@_3600_MHz_wiki-Japanese_language-ja_cpu.svg)

![g++ 9.1 C++17 Xeon E3-1230 Korean_language-ko](g++-9.1-17/fastest/g++_9.1_C++17_Xeon_E3-1230_f07_8_CPUs_@_3600_MHz_wiki-Korean_language-ko_cpu.svg)

![g++ 9.1 C++17 Xeon E3-1230 Portuguese_language-pt](g++-9.1-17/fastest/g++_9.1_C++17_Xeon_E3-1230_f08_8_CPUs_@_3600_MHz_wiki-Portuguese_language-pt_cpu.svg)

![g++ 9.1 C++17 Xeon E3-1230 Russian_language-ru](g++-9.1-17/fastest/g++_9.1_C++17_Xeon_E3-1230_f09_8_CPUs_@_3600_MHz_wiki-Russian_language-ru_cpu.svg)

![g++ 9.1 C++17 Xeon E3-1230 Swedish_language-sv](g++-9.1-17/fastest/g++_9.1_C++17_Xeon_E3-1230_f10_8_CPUs_@_3600_MHz_wiki-Swedish_language-sv_cpu.svg)

![g++ 9.1 C++17 Xeon E3-1230 Chinese_language-zh](g++-9.1-17/fastest/g++_9.1_C++17_Xeon_E3-1230_f11_8_CPUs_@_3600_MHz_wiki-Chinese_language-zh_cpu.svg)

## Summary

### High ASCII input

For ASCII-heavy input, the Clang compiler appears to favor ICU `U8_NEXT` as opposed to the MK ICU implementation, while the reverse is true of the GCC compiler. Furthermore, GCC appears to produce a somewhat faster implementation overall, with the MK ICU implementation in line with KEWB `FastBigTableConvert` and even the ICU `U8_NEXT_UNSAFE` implementations. The MK KEWB implementation performs more in line with the KEWB `BasicBigTableConvert`, which may be due to the fact that the MK implementation is actually a `decode_next` implementation which does not incorporate the outer loop directly.

### Medium ASCII input

The GCC compiler again favors the MK ICU implementation compared to the ICU `U8_NEXT` implementation, while the two are essentially even with the Clang compiler. GCC again yields an overall faster implementation which is ahead of KEWB `FastBigTableConvert` and just behind ICU `u_strFromUTF8Lenient`. Clang appears to produce a faster MK KEWB implementation, however, which also performs in line with KEWB `FastBigTableConvert`.

### Low ASCII input

Again, in most inputs with little ASCII, the GCC compiler favors the MK ICU implementation compared to the ICU `U8_NEXT` implementation, while the two are basically even when using Clang. In most cases, the MK ICU implementation performs on par with KEWB `FastBigTableConvert`.

## Conclusions

Overall, the MK ICU and KEWB `FastBigTableConvert` decoders perform the fastest for varying levels of ASCII input, especially with GCC for the former. If the API requires a `decode_next` implementation with some outer loop operating on each code point, for example, the MK ICU implementation is probably optimal. However, if all input can be decoded in one pass and processed in one pass, either the the MK ICU `decode_next` or KEWB `FastBigTableConvert` `decode` implementations are fastest, depending somewhat on the amount of ASCII input and compiler. Additionally, when the input is either predominately ASCII or valid UTF-8, the KEWB SSE vectorized and unsafe ICU implementations, respectively, can yield significant improvements.
