# libutf8decoders-bench-results

## Introduction

`libutf8decoders-bench-results` is a repository storing the benchmark results for `libutf8decoders`[1].

## Installation

To install, simply clone this repo:

```shell
git clone https://gitlab.com/mkrupcale/libutf8decoders-bench-results.git
cd libutf8decoders-bench-results
```

### Requirements

By itself this repository has no requirements, but running the benchmarks and analysis has the same requirements as those of `libutf8decoders`[2] and the benchmark and analysis scripts[3].

## Results

See [benchmark results](results/results.md) for some benchmark results.

## License

The contents of this repository are licensed under the MIT license, except where otherwise noted. Figures, in particular, are licensed under the Creative Commons Attribution-ShareAlike (CC BY-SA) 4.0 license.

## References

1. [`libutf8decoders`](https://gitlab.com/mkrupcale/libutf8decoders.git)
2. [`libutf8decoders` Requirements](https://gitlab.com/mkrupcale/libutf8decoders#requirements)
3. [`libutf8decoders` benchmark requirements](https://gitlab.com/mkrupcale/libutf8decoders#benchmarks)
